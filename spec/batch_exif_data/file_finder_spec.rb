require "spec_helper"

RSpec.describe BatchExifData::FileFinder do
  context "finds all the files properly" do
    let(:file_finder) { BatchExifData::FileFinder.new("#{__dir__}/..") }
    it{ expect(file_finder.files.length).to eq 5 }
  end
end
