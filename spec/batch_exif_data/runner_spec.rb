require "spec_helper"

RSpec.describe BatchExifData::Runner do
  context "performs properly" do
    let(:path) { "#{__dir__}/../gps_images" }
    let(:output_path) { "#{__dir__}/../../tmp/#{Time.now.to_i}.csv" }
    let(:runner) { BatchExifData::Runner.new(path: path, output_path: output_path) }
    let(:output_file) { File.new(output_path) }

    before { runner.perform }
    it { expect(output_file).to_not be_nil }
    it { expect(`wc -l < #{output_path}`.to_i).to eq 6}

  end

  context "default output path generated correctly" do
    # TODO implement
  end
end
