require "spec_helper"

RSpec.describe BatchExifData::Unit do
  context "handles a non-existent file properly" do
    let(:path) { "#{__dir__}/Canon_40D.jpg" }
    it { expect(BatchExifData::Unit.new(path).perform.error).not_to be_nil }
  end

  context "handles a valid file properly" do
    let(:path) { "#{__dir__}/../gps_images/image_a.jpg" }
    let(:result) { BatchExifData::Unit.new(path).perform }
    it { expect(result.error).to be_nil }
    it { expect(result.exif).not_to be_nil }
    it { expect(result.f_lat).to eq 50.09133333333333 }
    it { expect(result.f_lng).to eq -122.94566666666667 }
  end

  context "handles a file without exif file properly" do
    let(:path) { "#{__dir__}/../gps_images/image_b.jpg" }
    let(:result) { BatchExifData::Unit.new(path).perform }
    it { expect(result.error).not_to be_nil }
    it { expect(result.exif).to be_nil }
    it { expect(result.f_lat).to be_nil }
    it { expect(result.f_lng).to be_nil }
  end

  context "handles a valid file c properly" do
    let(:path) { "#{__dir__}/../gps_images/image_c.jpg" }
    let(:result) { BatchExifData::Unit.new(path).perform }
    it { expect(result.error).to be_nil }
    it { expect(result.exif).not_to be_nil }
    it { expect(result.f_lat).to eq 38.40 }
    it { expect(result.f_lng).to eq -122.82866666666666 }
  end

  context "handles a valid file d properly" do
    let(:path) { "#{__dir__}/../gps_images/image_d.jpg" }
    let(:result) { BatchExifData::Unit.new(path).perform }
    it { expect(result.error).to be_nil }
    it { expect(result.exif).not_to be_nil }
    it { expect(result.f_lat).to be_nil }
    it { expect(result.f_lng).to be_nil }
  end

  context "handles a valid file e without GPS coords properly" do
    let(:path) { "#{__dir__}/../gps_images/cats/image_e.jpg" }
    let(:result) { BatchExifData::Unit.new(path).perform }
    it { expect(result.error).to be_nil }
    it { expect(result.exif).not_to be_nil }
    it { expect(result.f_lat).to eq 59.9247550799827 }
    it { expect(result.f_lng).to eq 10.695598120067395 }
  end
end

