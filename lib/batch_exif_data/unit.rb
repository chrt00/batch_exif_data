# frozen_string_literal: true

require 'exif'

# BatchExifData
# A class to perform a single EXIF read.
# Designed this way so that it can be used on a background worker or separate thread
module BatchExifData
  class Unit
    attr_reader :path
    def initialize(path)
      @path = path
    end

    def perform
      exif = Exif::Data.new(File.new(path))
      Result.new(filename: path, exif: exif)
    rescue StandardError => e
      return Result.new(filename: path, exif: nil, error: e)
    end

    # TODO put this class into presenter with only necessary information
    class Result
      attr_reader :exif, :filename, :error
      def initialize(filename:, exif:, error: nil)
        @filename = filename
        @exif = exif
        @error = error
      end

      # TODO write test for this function
      def deg_lat
        return unless error.nil?
        format_degrees exif.gps_latitude, exif.gps_latitude_ref
      end

      # TODO write test for this function
      def deg_lng
        return unless error.nil?
        format_degrees exif.gps_longitude, exif.gps_longitude_ref
      end

      def f_lat
        return unless error.nil?
        format_decimal exif.gps_latitude, exif.gps_latitude_ref
      end

      def f_lng
        return unless error.nil?
        format_decimal exif.gps_longitude, exif.gps_longitude_ref
      end

      private
      def convert_rationals(array)
        array.map(&:to_f)
      end

      def format_degrees(array, direction)
        return if array.nil?
        a = convert_rationals(array)
        "#{a[0].to_i}° #{a[1]}' #{a[2]}\", #{direction}"
      end

      def format_decimal(array, direction)
        return if array.nil?
        a = convert_rationals(array)
        (a[0] + a[1]/60 + a[2]/3600) * direction_multiplier(direction)
      end

      # Get the right directionality
      # TODO check if gps_*_ref undercase is actually supported in EXIF spec
      def direction_multiplier(s)
        s.casecmp('W') == 0 || s.casecmp('S') == 0 ? -1 : 1
      end
    end
  end
end
