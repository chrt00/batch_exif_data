require 'csv'

module BatchExifData
  class Formatter
    class Csv < Formatter
      def write_io(io)
        csv = CSV.new(io)
        csv << %w[Filename,Latitude,Longitude,Error]
        @result_array.each do |e|
          csv << [e.filename, e.f_lat, e.f_lng, e.error&.message]
        end
      end
    end
  end
end