require 'nokogiri'

module BatchExifData
  class Formatter

    # Class for building html file - proof of concept
    # TODO cleanup nokogiri syntax, improvements, etc...
    class Html < Formatter
      def write_io(io)
        io << build_html.to_html
      end

      def build_html
        builder = Nokogiri::HTML::Builder.new do |doc|
          doc.html {
            doc.head {
              doc.title "images!"
            }
            doc.body {
              doc.table{
                doc.tr {
                  doc.th "path"
                  doc.th "lat"
                  doc.th "lng"
                  doc.th "error"
                }
                @result_array.each do |r|
                  doc.tr {
                    doc.td r.filename
                    doc.td r.f_lat
                    doc.td r.f_lng
                    doc.td r.error&.message
                  }
                end
              }
            }
          }
        end
      end
    end
  end
end