module BatchExifData
  class Formatter
    def initialize(result_array:)
      @result_array = result_array
    end

    # TODO investigate if this is the right paradigm to implement the formatter, not investigated with nokogiri
    def write_io(io)
      raise NotImplementedError
    end
  end
end
