module BatchExifData
  class FileFinder
    attr_reader :path, :formats
    def initialize(path, formats: %w[jpg, jpeg])
      @path = path
      @formats = formats
    end

    def files
      Dir["#{path}/**/*.{#{formats.join(",")}}"]
    end
  end
end