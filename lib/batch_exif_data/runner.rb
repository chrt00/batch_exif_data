# frozen_string_literal: true

# A naive single-threaded approach to processing image files
module BatchExifData
  class Runner
    def initialize(path:, output_format: :csv, output_path:)
      @path = path
      klass_name = camelize(output_format.to_s)
      @formatter_klass = BatchExifData::Formatter.const_get(klass_name)
      @output_path = Dir.exist?(output_path) ? "#{output_path}.#{output_format.to_s}" : output_path
    end

    def perform
      results = results(files_to_process)
      formatter = @formatter_klass.new(result_array: results)
      File.open(@output_path, "w") do |file|
        formatter.write_io(file)
      end
    end

    private
    def results(files)
      files.map { |file_path| Unit.new(file_path).perform }
    end

    def files_to_process
      FileFinder.new(@path).files.sort
    end

    def camelize(string, uppercase_first_letter = true)
      if uppercase_first_letter
        string = string.sub(/^[a-z\d]*/) { $&.capitalize }
      else
        string = string.sub(/^(?:(?=\b|[A-Z_])|\w)/) { $&.downcase }
      end
      string.gsub(/(?:_|(\/))([a-z\d]*)/) { "#{$1}#{$2.capitalize}" }.gsub('/', '::')
    end
  end
end
