# Batch::Exif::Data

A command line application that recursively reads all of the images from the supplied directory of images, extracts their EXIF GPS data (longitude and latitude), and then writes the name of that image and any GPS co-ordinates it finds to a CSV file.

### TODO
 - Fix executable to be usable from anywhere other than gem root dir
 - Add to $PATH on gem install
 - Publish on Github
 - Add rubocop linting
 - RDoc
 - Increase test coverage, formatters, CLI have no coverage
 - HTML formatter nokogiri generation needs cleanup
 - Fix `//` in result pathname, when input path has trailing `/` - may be because of Ruby globbing
 
## Usage

Currently you must use the utility within the gem root dir.
If you plan on using the CLI, clone the repo and run it from the code source.

### Command line
Make sure you have installed `libexif` and `bundle install` first.

```
bin/batch_exif_data
```

```sh
Usage: batch_exif_data [options] [path]
    -o, --output OUTPUT              The name of output
    -f, --format FORMAT              The format to output, [csv, html]
```

example:
```
#:~/batch_exif_data$ bin/batch_exif_data --format html --output a.html ~/Downloads/
Batch EXIF GPS coordinate processing...
{:output_format=>"html", :output_path=>"a.html"}
{:output_format=>"html", :output_path=>"a.html", :path=>"~/Downloads/"}
```
```
#:~/batch_exif_data$ bin/batch_exif_data --format csv --output a.csv ~/Downloads/
Batch EXIF GPS coordinate processing...
{:output_format=>"csv", :output_path=>"a.csv"}
{:output_format=>"csv", :output_path=>"a.csv", :path=>"~/Downloads/"}
```
```
#:~/batch_exif_data$ bin/batch_exif_data --format csv ~/Downloads/
Batch EXIF GPS coordinate processing...
{:output_format=>"csv"}
{:output_format=>"csv", :path=>"~/Downloads/", :output_path=>"output.csv"}
```
```
#:~/batch_exif_data$ bin/batch_exif_data --format html ~/Downloads/
Batch EXIF GPS coordinate processing...
{:output_format=>"html"}
{:output_format=>"html", :path=>"~/Downloads/", :output_path=>"output.html"}

```
## Installation

Make sure you have `libexif` installed on your machine first
```
$ brew install libexif             # Homebrew
$ sudo apt-get install libexif-dev # APT
$ sudo yum install libexif-devel   # CentOS
```

Add this line to your application's Gemfile:

```ruby
gem 'batch-exif-data'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install batch-exif-data


## Development



## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/chrt00/batch-exif-data.
